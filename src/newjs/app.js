import Plyr from 'plyr';

const videoControls = `
<div class="plyr__controls">
    <button type="button" class="plyr__controls__item plyr__control" aria-label="Play, {title}" data-plyr="play">
        <svg class="icon--pressed" role="presentation" xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 16 20"><defs><style>.a{fill:#ff9c41;}</style></defs><g transform="translate(-1079 -525)"><g transform="translate(1079 525)"><rect class="a" width="4" height="20"/><rect class="a" width="4" height="20" transform="translate(12)"/></g></g></svg>
        <span class="label--pressed plyr__tooltip" role="tooltip">Pause</span>
    </button>
    <div class="plyr__progress">
        <input data-plyr="seek" type="range" min="0" max="100" step="0.01" value="0" aria-label="Seek">
        <progress class="plyr__progress__buffer" min="0" max="100" value="0">% buffered</progress>
        <span role="tooltip" class="plyr__tooltip">00:00</span>
    </div>
    <button class="video-player__sound">
         <div class="volume">
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="94" viewBox="0 0 30 94"><g transform="translate(-1085 -4469)"><rect class="a" width="30" height="94" rx="15" transform="translate(1085 4469)"/><g class="b" transform="translate(1085 4469)"><rect class="d" width="30" height="94" rx="15"/><rect class="e" x="0.5" y="0.5" width="29" height="93" rx="14.5"/></g><rect class="c" width="14" height="2" transform="translate(1093 4546)"/><g transform="translate(1 1)"><rect class="c" width="14" height="2" transform="translate(1092 4484)"/><rect class="c" width="14" height="2" transform="translate(1100 4478) rotate(90)"/></g></g></svg>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="19.999" viewBox="0 0 23 19.999"><g transform="translate(-0.002 -0.001)"><path class="a" d="M11.189,19.807c-.025-.015-.046-.033-.071-.051L5.132,14.345l-4.693,0a.458.458,0,0,1-.311-.127A.425.425,0,0,1,0,13.911V5.652a.435.435,0,0,1,.439-.433h4.7L11.133.234a.283.283,0,0,1,.056-.04,1.372,1.372,0,0,1,2.073,1.176V18.627A1.372,1.372,0,0,1,11.89,20,1.391,1.391,0,0,1,11.189,19.807Zm-5.443-8.07v1.985l5.934,5.356a.453.453,0,0,0,.442-.011.488.488,0,0,0,.254-.44V1.369a.488.488,0,0,0-.254-.437.472.472,0,0,0-.45-.01L5.746,5.853V7.825a.441.441,0,0,1-.883,0V6.086H.882v7.393H4.864V11.737a.441.441,0,0,1,.883,0ZM16.6,17.531a.431.431,0,0,1,.275-.55,7.561,7.561,0,0,0-.011-14.4.434.434,0,0,1-.275-.553.444.444,0,0,1,.559-.27A8.426,8.426,0,0,1,17.164,17.8a.468.468,0,0,1-.142.022A.443.443,0,0,1,16.6,17.531Zm-2.44-2.41a.44.44,0,0,1,.306-.539,4.974,4.974,0,0,0,0-9.6.435.435,0,0,1-.306-.538.453.453,0,0,1,.549-.3,5.913,5.913,0,0,1,4.313,5.638,5.909,5.909,0,0,1-4.313,5.637.462.462,0,0,1-.124.014A.448.448,0,0,1,14.161,15.12Z" transform="translate(0.002 0.001)"/></g></svg>
    </button>
    
    <!--<button type="button" class="plyr__control" aria-label="Mute" data-plyr="mute">
        <svg class="icon--pressed" role="presentation"><use xlink:href="#plyr-muted"></use></svg>
        <svg class="icon--not-pressed" role="presentation"><use xlink:href="#plyr-volume"></use></svg>
        <span class="label--pressed plyr__tooltip" role="tooltip">Unmute</span>
        <span class="label--not-pressed plyr__tooltip" role="tooltip">Mute</span>
    </button>-->
    <!--<div class="plyr__volume">
        <input data-plyr="volume" type="range" min="0" max="1" step="0.05" value="1" autocomplete="off" aria-label="Volume">
    </div>-->
    <button type="button" class="plyr__control" data-plyr="captions">
        <svg class="icon--pressed" role="presentation"><use xlink:href="#plyr-captions-on"></use></svg>
        <svg class="icon--not-pressed" role="presentation"><use xlink:href="#plyr-captions-off"></use></svg>
        <span class="label--pressed plyr__tooltip" role="tooltip">Disable captions</span>
        <span class="label--not-pressed plyr__tooltip" role="tooltip">Enable captions</span>
    </button>
    <button type="button" class="plyr__control video-player__fullscreen" data-plyr="fullscreen">
        <svg class="video-player__fullscreen-icon" role="presentation" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><style>.a{fill:#ff9c41;}</style></defs><g transform="translate(0 -2.95)"><g transform="translate(0 2.95)"><path class="a" d="M.185,9.789.1,3.525A.566.566,0,0,1,.667,2.95l6.188.091a.568.568,0,0,1,.389.969L5.522,5.753l2.863,2.9L5.727,11.341l-2.863-2.9L1.142,10.186A.562.562,0,0,1,.185,9.789Z" transform="translate(-0.095 -2.95)"/><path class="a" d="M291.821,9.439l1.722,1.743a.56.56,0,0,0,.957-.393l.09-6.264a.566.566,0,0,0-.568-.575l-6.188.091a.568.568,0,0,0-.389.969l1.718,1.735-2.863,2.9,2.658,2.691Z" transform="translate(-274.59 -3.908)"/><path class="a" d="M287.345,290.779a.568.568,0,0,0,.389.969l6.188.091a.566.566,0,0,0,.569-.575L294.4,285a.56.56,0,0,0-.957-.393l-1.722,1.743-2.863-2.9-2.658,2.691,2.863,2.9Z" transform="translate(-274.494 -271.839)"/><path class="a" d="M.573,290.838l6.188-.091a.568.568,0,0,0,.389-.969L5.427,288.04l2.863-2.9-2.658-2.691-2.863,2.9-1.722-1.743A.56.56,0,0,0,.09,284L0,290.263A.572.572,0,0,0,.573,290.838Z" transform="translate(0 -270.88)"/></g></g></svg>
        <span class="label--pressed plyr__tooltip" role="tooltip">Exit fullscreen</span>
        <span class="label--not-pressed plyr__tooltip" role="tooltip">Enter fullscreen</span>
    </button>
</div>
<button type="button" class="plyr__control--overlaid video-player__play-button js-video-player__play-button" data-plyr="play" aria-label="Play">
    <svg class="video-player__play-button-icon icon--not-pressed" role="presentation" xmlns="http://www.w3.org/2000/svg" width="110" height="110" viewBox="0 0 110 110"><g transform="translate(-1031 -480)"><circle class="a" cx="55" cy="55" r="55" transform="translate(1031 480)"/><path class="b" d="M18,0V18H0Z" transform="translate(1072.001 534.729) rotate(-45)"/></g></svg>
</button>
`;





const audioControls = `
<div class="plyr__controls">
    <button type="button" class="audio-player__play plyr__controls__item plyr__control" aria-label="Play, {title}" data-plyr="play">
        <svg class="audio-player__play-icon icon--not-pressed" role="presentation" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><defs><style>.a{fill:#ff9c41;}.b{fill:#333;}</style></defs><g transform="translate(-1061 -507)"><circle class="a" cx="25" cy="25" r="25" transform="translate(1061 507)"/><path class="b" d="M10,0V10H0Z" transform="translate(1078 532.071) rotate(-45)"/></g></svg>    
        <div class="audio-player__pause icon--pressed">
            <svg class="audio-player__pause-icon" role="presentation" xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 16 20"><defs><style>.a{fill:#ff9c41;}</style></defs><g transform="translate(-1079 -525)"><g transform="translate(1079 525)"><rect class="a" width="4" height="20"/><rect class="a" width="4" height="20" transform="translate(12)"/></g></g></svg>        
        </div>
        <span class="label--pressed plyr__tooltip" role="tooltip">Pause</span>
        <span class="label--not-pressed plyr__tooltip" role="tooltip">Play</span>
    </button>
    
    <!--<div class="plyr__progress">
        <input data-plyr="seek" type="range" min="0" max="100" step="0.01" value="0" aria-label="Seek">
        <progress class="plyr__progress__buffer" min="0" max="100" value="0">% buffered</progress>
    </div>-->
    <button class="audio-player__sound">
        <svg class="audio-player__sound-icon" xmlns="http://www.w3.org/2000/svg" width="23" height="19.999" viewBox="0 0 23 19.999"><g transform="translate(-0.002 -0.001)"><path class="a" d="M11.189,19.807c-.025-.015-.046-.033-.071-.051L5.132,14.345l-4.693,0a.458.458,0,0,1-.311-.127A.425.425,0,0,1,0,13.911V5.652a.435.435,0,0,1,.439-.433h4.7L11.133.234a.283.283,0,0,1,.056-.04,1.372,1.372,0,0,1,2.073,1.176V18.627A1.372,1.372,0,0,1,11.89,20,1.391,1.391,0,0,1,11.189,19.807Zm-5.443-8.07v1.985l5.934,5.356a.453.453,0,0,0,.442-.011.488.488,0,0,0,.254-.44V1.369a.488.488,0,0,0-.254-.437.472.472,0,0,0-.45-.01L5.746,5.853V7.825a.441.441,0,0,1-.883,0V6.086H.882v7.393H4.864V11.737a.441.441,0,0,1,.883,0ZM16.6,17.531a.431.431,0,0,1,.275-.55,7.561,7.561,0,0,0-.011-14.4.434.434,0,0,1-.275-.553.444.444,0,0,1,.559-.27A8.426,8.426,0,0,1,17.164,17.8a.468.468,0,0,1-.142.022A.443.443,0,0,1,16.6,17.531Zm-2.44-2.41a.44.44,0,0,1,.306-.539,4.974,4.974,0,0,0,0-9.6.435.435,0,0,1-.306-.538.453.453,0,0,1,.549-.3,5.913,5.913,0,0,1,4.313,5.638,5.909,5.909,0,0,1-4.313,5.637.462.462,0,0,1-.124.014A.448.448,0,0,1,14.161,15.12Z" transform="translate(0.002 0.001)"/></g></svg>
        <div class="volume">
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="94" viewBox="0 0 30 94"><g transform="translate(-1085 -4469)"><rect class="a" width="30" height="94" rx="15" transform="translate(1085 4469)"/><g class="b" transform="translate(1085 4469)"><rect class="d" width="30" height="94" rx="15"/><rect class="e" x="0.5" y="0.5" width="29" height="93" rx="14.5"/></g><rect class="c" width="14" height="2" transform="translate(1093 4546)"/><g transform="translate(1 1)"><rect class="c" width="14" height="2" transform="translate(1092 4484)"/><rect class="c" width="14" height="2" transform="translate(1100 4478) rotate(90)"/></g></g></svg>
        </div>
    </button>
    <button type="button" class="plyr__control" data-plyr="captions">
        <svg class="icon--pressed" role="presentation"><use xlink:href="#plyr-captions-on"></use></svg>
        <svg class="icon--not-pressed" role="presentation"><use xlink:href="#plyr-captions-off"></use></svg>
        <span class="label--pressed plyr__tooltip" role="tooltip">Disable captions</span>
        <span class="label--not-pressed plyr__tooltip" role="tooltip">Enable captions</span>
    </button>
</div>
`;
document.addEventListener("DOMContentLoaded", () => {
  const videoNode = document.querySelector('#video-player');
  const video  = new Plyr(videoNode, { controls: videoControls });

  const audioPlayers = document.querySelectorAll('.audio-player audio');
  audioPlayers.forEach(item => new Plyr(item, { controls: audioControls }));
});
